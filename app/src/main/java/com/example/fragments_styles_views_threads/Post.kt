package com.example.fragments_styles_views_threads

data class Post(val text: String, var style: Int = R.style.post_style_1)
