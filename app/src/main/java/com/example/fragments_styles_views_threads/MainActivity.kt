package com.example.fragments_styles_views_threads

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import com.example.fragments_styles_views_threads.databinding.ActivityMainBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

interface BottomBarEnabler {
    fun setNavbarVisibility(visible: Boolean)
}

class MainActivity : AppCompatActivity(), BottomBarEnabler {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val bottomNavigationView: BottomNavigationView = binding.bottomNavigationView

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        val navController = navHostFragment.navController


        bottomNavigationView.setOnItemSelectedListener { menuItem ->
            var route: Int? = null
            val possibleToNavigate: (Int, Int) -> Boolean = {menuId, fragment ->
                menuItem.itemId == menuId && navController.currentDestination?.id != fragment}

            if (possibleToNavigate(R.id.mi_web, R.id.webFragment))
                route = R.id.navigateToWebFragment
            if (possibleToNavigate(R.id.mi_home, R.id.homeFragment))
                route = R.id.navigateToHomeFromWeb

            if (route != null) navController.navigate(route)
            true
        }
    }

    override fun setNavbarVisibility(visible: Boolean) {
        binding.bottomNavigationView.visibility = if (visible) View.VISIBLE else View.INVISIBLE
    }
}