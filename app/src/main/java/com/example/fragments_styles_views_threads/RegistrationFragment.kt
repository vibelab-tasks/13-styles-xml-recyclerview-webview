package com.example.fragments_styles_views_threads

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.fragments_styles_views_threads.databinding.FragmentRegistrationBinding


class RegistrationFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentRegistrationBinding.inflate(layoutInflater)

        val view = binding.root

        binding.btnBackToLogin.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.navigateToLoginFromRegistration)
        }

        return view
    }

    companion object {
        fun newInstance() =
            RegistrationFragment()
    }
}