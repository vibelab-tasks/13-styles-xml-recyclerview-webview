package com.example.fragments_styles_views_threads

import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.navigation.Navigation
import com.example.fragments_styles_views_threads.databinding.FragmentLoginBinding


class LoginFragment : Fragment() {

    lateinit var bottomBarEnabler: BottomBarEnabler

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BottomBarEnabler) {
            bottomBarEnabler = context
        } else {
            throw RuntimeException("MainActivity must implement BottomBarEnabler!")
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bottomBarEnabler.setNavbarVisibility(false)

        val binding = FragmentLoginBinding.inflate(layoutInflater)
        val view = binding.root


        binding.btnSignUp.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.navigateToRegistrationFragment)
        }

        binding.btnLogin.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.navigateToHomeFromLogin)
            bottomBarEnabler.setNavbarVisibility(true)
        }

        return view
    }

    companion object {

        fun newInstance() = LoginFragment()

    }
}