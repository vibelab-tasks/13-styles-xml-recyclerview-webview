package com.example.fragments_styles_views_threads

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.fragments_styles_views_threads.databinding.FragmentWebBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

class WebFragment : Fragment() {
    private lateinit var binding: FragmentWebBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWebBinding.inflate(layoutInflater)

        val view = binding.root

        webViewSetup()
        
        requireActivity()
            .onBackPressedDispatcher
            .addCallback(this, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (binding.webView.canGoBack())
                        binding.webView.goBack()
                }
            }
            )

        return view
    }

    private fun webViewSetup() = with(binding) {
        webView.webViewClient = WebViewClient()
        webView.apply {
            loadUrl("https://www.google.com/")
        }
    }

    companion object {
        fun newInstance() =
            WebFragment()
    }
}