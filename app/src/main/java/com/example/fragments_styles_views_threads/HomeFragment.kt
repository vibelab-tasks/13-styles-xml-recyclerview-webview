package com.example.fragments_styles_views_threads

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fragments_styles_views_threads.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private val postAdapter = PostAdapter()
    private lateinit var mainHandler: Handler


    override fun onDestroyView() {
        super.onDestroyView()
        mainHandler.removeCallbacksAndMessages(null)
        println("debug: Home destroyed")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(layoutInflater)
        val view = binding.root

        requireActivity()
            .onBackPressedDispatcher
            .addCallback(this, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {}
            }
            )

        initRecycleView()

        mainHandler = Handler(Looper.getMainLooper())
        val delay = 3000L
        var count = 0

        val styles = listOf(R.style.post_style_1, R.style.post_style_2, R.style.post_style_3)
        postAdapter.addPost(Post("Welcome to my blog! There you'll find some meaningless " +
                "posts "))

        mainHandler.post(object : Runnable {
            override fun run() {
                postAdapter.addPost( Post("string #$count") )
                for(i in 0 until postAdapter.itemCount)
                    postAdapter.changePostStyle(i, styles[count % 3])
                count ++
                mainHandler.postDelayed(this, delay)

                println("debug: $count")
            }
        })


        return view
    }

    private fun initRecycleView() = with(binding) {
        rvStringListing.layoutManager = LinearLayoutManager(this@HomeFragment.context)
        rvStringListing.adapter = postAdapter
    }

    companion object {

        fun newInstance() =
            HomeFragment()
    }
}